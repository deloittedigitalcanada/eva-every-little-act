import React from 'react';
import facebook from "../../assets/img/facebook.svg";
import twitter from "../../assets/img/twitter.svg";
import './style.scss';

function Footer() {
  const rootClass = 'footer';
  return (
    <footer className={rootClass}>
      <div className={`${rootClass}__left-container`}>
        <a href='/learn-more' className={`${rootClass}__link`}>Learn more about the cards</a>
        <div className={`${rootClass}__share`}>
          <span>Share this on</span>
          <div className={`${rootClass}__social`}>
            <a href="/#">
              <img src={facebook} alt="Link to Eva's Facebook page"/>
            </a>
            <a href="/#">
              <img src={twitter} alt="Link to Eva's Twitter page"/>
            </a>
          </div>
        </div>
      </div>
        <span className={`${rootClass}__copyright`}>© 2020 Eva’s, Deloitte</span>
    </footer>
  );
}

export default Footer;
