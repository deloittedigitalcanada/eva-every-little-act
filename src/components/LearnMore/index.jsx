import React from 'react';
import Button from '../Button';
import Footer from '../Footer';
import illustration from '../../assets/img/cards-icon.svg';
import evaLogo from '../../assets/img/eva-logo.svg';
import deloitteLogo from '../../assets/img/deloitte-doblin-logos.svg';
import deloitteLogoMobile from '../../assets/img/deloitte-doblin-logos-mobile.svg';
import "./style.scss"

function LearnMore(){
  const rootClass = 'learn-more';
    return(
    <>
        <div className={`${rootClass}__link`}>
            <a href='/'>Take me back to the home page</a>
        </div>
        <h1 className={`${rootClass}__h1`}>Where did these cards come from?</h1>
        <div className={`${rootClass}__img`}>
            <img
                src={illustration}
                alt=""
                aria-hidden
            />
        </div>
        <div className={`${rootClass}__body`}>
            Every Little Act is adapted from an initiative that started in 2019. As part of the Canadian National Housing Strategy, Eva’s Initiatives for Homeless Youth and Deloitte Canada ran a research project to better understand the journeys of young people as they experience housing precarity and homelessness.
        </div>
        <div className={`${rootClass}__body`}>
            As part of this research, the team conducted interviews with young people between the ages of 16 to 24, who had experiences of moving in and out of homelessness. Participants shared their stories, and with the help of the cards, were able to reframe their experiences as acts of resilience. These cards were a collaborative effort developed with the help of frontline staff, caseworkers, academic researchers, and people with lived experiences of homelessness and housing precarity.
        </div>
        <div className={`${rootClass}__body`}>
            While the version of the cards you see on this website are slightly different than the originals, they are similar in that they are a way for us to reframe the ways in which we cope with adversity. They can enable us to see our struggles as badges of honour, our hardship as a source of resilience.
        </div>

    <div className={`${rootClass}__company-container`}>
        <div className={`${rootClass}__eva`}>
          <img
            src={evaLogo}
            alt='Eva logo'
            />
            <div>
                Eva’s is an award-winning organization that provides shelter, transitional housing, and programming to help homeless and at-risk youth reach their potential to lead productive, self-sufficient, and healthy lives. Eva's operates various facilities across the city of Toronto, providing safe shelter, transitional housing, and programs for 123 young people every day.
            </div>
            <Button title="Visit Eva’s website"/>
        </div>
        <div className={`${rootClass}__deloitte`}>
              <div className="desktopOnly">
              <img
                src={deloitteLogo}
                alt='Deloitte and Doblin logos'

              />
              </div>
              <div className="mobileOnly">
                  <img
                src={deloitteLogoMobile}
                alt='Deloitte and Doblin logos'

              />
              </div>
            <div>
                Deloitte is a multinational professional services network that offers accounting, tax, consulting, and advisory services. The group within Deloitte that conducted the research alongside Eva's is called Doblin. Doblin is a design innovation consultancy that helps public and private organizations better understand their users, and develop innovation strategies.
            </div>
            <div className={`${rootClass}__deloitte-buttons`}>
            <Button title="Visit Deloitte’s website"/>
            <Button title="Visit Doblin’s website"/>
            </div>
        </div>
        </div>
        <Footer/>
    </>
    )
}

export default LearnMore;