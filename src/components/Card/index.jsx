import React, { useState, useEffect } from 'react';
import './style.scss';

function Card(props) {
const rootClass = 'card';
  var [selected, setSelected] = useState(false);

  useEffect(() => {
    setSelected(false);
  }, [props.reset]);

  return (
    <div className={`${rootClass}__container`}>
      <div
        className={selected ? `${rootClass} ${rootClass}__selected` : `${rootClass}`}
        tabIndex='0'
        onClick={() => setSelected(!selected)}
        onKeyPress={() => setSelected(!selected)}
      >
        {props.cardTitle}
      </div>
    </div>
  );
}

export default Card;
