import React, { Component } from 'react';
import Lottie from 'react-lottie';
import animationData from '../../assets/lotties/personal.json';

class LoremLottie extends Component {
  render() {
    const defaultOptions = {
      loop: false,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
      },
    };

    return (
        <Lottie
          options={defaultOptions}
        />
    );
  }
}

export default LoremLottie;
