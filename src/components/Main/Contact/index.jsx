import React from 'react';
import blob from "../../../assets/img/contact-blob.svg";
import Button from "../../Button"
import './style.scss';

function Contact(){
  const rootClass = 'contact';
    return(
        <section className={rootClass}>
            <div className={`${rootClass}__img-container`}>
                <img src={blob} aria-hidden alt="" className={`${rootClass}__img`} />
            </div>

            <h1 className={`${rootClass}__h1`}>Get in touch with us</h1>
            <div className={`${rootClass}__body`}>
                <span>We want to hear what you think about our tool and how we can improve it to make it better and if you like our cards, feel free to donate to us so we can build more tools like this one.</span>
            </div>
            <div className={`${rootClass}__button-container`}>
                <Button title="E-mail us" />
                <Button title="Donate to Eva's" />
            </div>
        </section>
    )
}

export default Contact;