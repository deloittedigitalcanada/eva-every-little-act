import React from 'react';
import EveryLittleAct from "./EveryLittleAct";
import HowToUse from "./HowToUse";
import WhichCards from "./WhichCards";
import Contact from "./Contact"
import Footer from "../Footer";

function Main(){
    return(
        <main>
            <EveryLittleAct/>
            <HowToUse/>
            <WhichCards/>
            <Contact/>
            <Footer/>
        </main>
    )
}

export default Main;