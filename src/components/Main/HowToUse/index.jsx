import {Waypoint} from 'react-waypoint';
import React, {useState} from 'react';
import FamilyDinnerLottie from '../../Lottie/FamilyDinnerLottie';
import PersonalReflectionLottie from '../../Lottie/PersonalReflectionLottie';
import TeamMeetingLottie from '../../Lottie/TeamMeetingLottie';
import FamilyDinnerLottieMobile from '../../Lottie/FamilyDinnerLottieMobile';
import PersonalReflectionLottieMobile from '../../Lottie/PersonalReflectionLottieMobile';
import TeamMeetingLottieMobile from '../../Lottie/TeamMeetingLottieMobile';
import Fade from 'react-reveal/Fade';
import './style.scss';

function HowToUse() {

  let [renderPersonal, setRenderPersonal] = useState(false);
  let [renderTeam, setRenderTeam] = useState(false);
  let [renderFamily, setRenderFamily] = useState(false);
  let [renderPersonalMobile, setRenderPersonalMobile] = useState(false);
  let [renderTeamMobile, setRenderTeamMobile] = useState(false);
  let [renderFamilyMobile, setRenderFamilyMobile] = useState(false);

  const rootClass = 'how-to';
    return(
        <section className={rootClass}>
        <h3 className={`${rootClass}__h3`}>You can use them in a few ways...</h3>
        <Fade bottom>
        <div className={`${rootClass}__section-container`}>
            <div className={`${rootClass}__lottie-mobile`}>
            <Waypoint
                onEnter={()=>setRenderPersonalMobile(true)}
              />
              { renderPersonalMobile &&
                            <PersonalReflectionLottieMobile />}

          </div>

            <div className={`${rootClass}__text-container`}>
            <div className={`${rootClass}__h4`}>
              As personal reflection
            </div>
            <div className={`${rootClass}__body`}>
            At the end of your day (or week), take a moment to read through the cards and select a few that highlight the decisions you made to protect yourself or others.
            </div>
          </div>
            <div className={`${rootClass}__lottie`}>
              <Waypoint
                onEnter={()=>setRenderPersonal(true)}
              />
              { renderPersonal &&
                <PersonalReflectionLottie />}
          </div>
            </div>
        </Fade>
        <Fade bottom>
        <div className={`${rootClass}__section-container`}>
            <div className={`${rootClass}__lottie-mobile`}>
            <Waypoint
                onEnter={()=>setRenderTeamMobile(true)}
              />
              { renderTeamMobile &&
      <TeamMeetingLottieMobile />}

            </div>
            <div className={`${rootClass}__lottie`}>
            <Waypoint
                onEnter={()=>setRenderTeam(true)}
              />
              { renderTeam &&
                           <TeamMeetingLottie />}
            </div>
            <div className={`${rootClass}__text-container`}>
            <div className={`${rootClass}__h4`}>
            During a team meeting
            </div>
            <div className={`${rootClass}__body`}>
            Start your team meeting by taking a few minutes for each person to select a card that resonates with them. Go around and ask people to share their acts of resilience.
            </div>
            </div>
            </div>
        </Fade>
        <Fade bottom>
        <div className={`${rootClass}__section-container`}>
            <div className={`${rootClass}__lottie-mobile`}>
            <Waypoint
                onEnter={()=>setRenderFamilyMobile(true)}
              />
              { renderFamilyMobile &&
                <FamilyDinnerLottieMobile />}
            </div>
            <div className={`${rootClass}__text-container`}>
            <div className={`${rootClass}__h4`}>
            After a family dinner
            </div>
            <div className={`${rootClass}__body`}>
            As a family look through the cards and pick 3–5 that show how you are getting through this time together.
            </div>
          </div>
            <div className={`${rootClass}__lottie`}>
            <Waypoint
                onEnter={()=>setRenderFamily(true)}
              />
              { renderFamily &&
                  <FamilyDinnerLottie />}

            </div>
          </div>
          </Fade>
        </section>
    )
}

export default HowToUse;