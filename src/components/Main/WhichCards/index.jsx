import React, { useState } from 'react';
import Card from '../../Card';
import Button from '../../Button';
import cards from '../../../assets/cards.json';
import './style.scss';

function WhichCards() {
const rootClass = 'which-cards';
  var [reset, setReset] = useState(0);
  return (
    <section className={rootClass}>
      <h3 className={`${rootClass}__h3`}>Which little acts have you done today?</h3>
      <div className={`${rootClass}__body`}>
          Read through each one and pick the ones that you’ve done today and
          reflect on your actions.
      </div>
      <div className={`${rootClass}__link`}>
        <a href='/#'>Download cards as PDF</a>
      </div>
      <div className={`${rootClass}__container`}>
        {cards['Cards'].map((card, i) => (
          <Card key={i} cardTitle={card} reset={reset} />
        ))}
      </div>
     <div className={`${rootClass}__empty-card`}>
      You can also add your own little act by writing it down on a piece of paper
     </div>
      <div className={`${rootClass}__button-container`}>
      <Button onClick={() => setReset(reset + 1)} title="Clear selection" />
      </div>
    </section>
  );
}

export default WhichCards;