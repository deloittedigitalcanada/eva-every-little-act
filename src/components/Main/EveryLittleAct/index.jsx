import React from 'react';
import logo from "../../../assets/img/eva-logo.svg";
import flower from "../../../assets/img/flower-icon.svg";
import './style.scss';

function EveryLittleAct(){
  const rootClass = 'every-little-act';
    return(
        <section className={rootClass}>
        <div className={`${rootClass}__img`}>
            <img src={logo} alt='Eva logo' />
        </div>
        <h1 className={`${rootClass}__h1`}>Every Little Act</h1>
        <h2 className={`${rootClass}__h2`}>Your acts of resiliency</h2>
        <div className={`${rootClass}__img`}>
            <img
                src={flower}
                alt=""
                aria-hidden
            />
        </div>
        <div className={`${rootClass}__body`}>
           Let's face it— these are not easy times. With the COVID-19 pandemic in full force, it can be difficult to remain positive. We're reading one distressing news story after another, struggling to adapt to an unprecedented "new normal", and, most critically, forgetting to recognize the little things we do every day.
        </div>
        <div className={`${rootClass}__body`}>
            What if, instead of focusing on our shortcomings in this time of crisis, we celebrated our efforts to cope?
        </div>
        <div className={`${rootClass}__body`}>
            These resiliency cards can help us do just that.
        </div>
        <div className={`${rootClass}__body`}>
            Take a look through them. Use them as a reminder to be kind to yourself, and to acknowledge your own efforts in these challenging times. You may not think you have done a lot to help yourself and others, but you have.
        </div>
        </section>
    )
}

export default EveryLittleAct;