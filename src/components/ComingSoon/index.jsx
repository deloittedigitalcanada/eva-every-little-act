import React from 'react';
import logo from "../../assets/img/eva-logo.svg";
import illustration from "../../assets/img/coming-soon.svg";
import './style.scss';

function ComingSoon(){
  const rootClass = 'coming-soon';
    return(
        <section className={rootClass}>
        <div className={`${rootClass}__img`}>
            <img src={logo} alt='Eva logo' />
        </div>
        <h1 className={`${rootClass}__h1`}>Every Little Act</h1>
        <h2 className={`${rootClass}__h2`}>Coming soon</h2>
        <div className={`${rootClass}__img`}>
            <img
                src={illustration}
                alt=""
                aria-hidden
            />
        </div>
        </section>
    )
}

export default ComingSoon;