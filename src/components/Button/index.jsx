
import React from 'react';
import './style.scss';

function Card(props) {
const rootClass = 'button';
  return (
    <div className={`${rootClass}__container`}>
      <button href={props.href} onClick={props.onClick} className={`${rootClass}`}>{props.title}</button>
    </div>
  );
}

export default Card;

