// TO DO: remove coming soon, and uncomment main and learn more

import React from 'react';
import ComingSoon from './components/ComingSoon';
// import Main from './components/Main';
// import LearnMore from './components/LearnMore';
import './scss/master.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Route
        path='/'
        exact
        render={() => {
          return <ComingSoon />;
          // return <Main />;
        }}
      />
      {/* <Route
        path='/learn-more'
        exact
        render={() => {
          return <LearnMore />;
        }}
      /> */}
    </Router>
  );
}

export default App;
